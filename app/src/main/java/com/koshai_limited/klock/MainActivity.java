package com.koshai_limited.klock;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {
    private Button btn1, btn2;
    int clk1 = 0, clk2 = 0, inc = 0;
    private CountDownTimer cnt1, cnt2;
    private ProgressBar prg1, prg2;
    double run1, run2;
    private int sec_time, time;
    long store1, store2;
    private TextView txt1, txt2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        setContentView(R.layout.activity_main);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setHomeButtonEnabled(false);
        }

        btn1 = findViewById(R.id.btn_1);
        btn2 = findViewById(R.id.btn_2);
        txt1 = findViewById(R.id.txt_1);
        txt2 = findViewById(R.id.txt_2);
        prg1 = findViewById(R.id.prg_1);
        prg2 = findViewById(R.id.prg_2);

        Bundle bundle = getIntent().getExtras();
        String str1 = bundle.getString("min");
        String str2 = bundle.getString("sec");
        time = Integer.parseInt(str1) * 60000;
        sec_time = Integer.parseInt(str2) * 1000;
        store1 = time;
        store2 = time;
        txt1.setText("00:" + str1 + ":00");
        txt2.setText("00:" + str1 + ":00");

        btn2.setOnClickListener(v -> {
            btn2.setEnabled(false);
            btn1.setEnabled(true);
            inc++;
            if(store1 < 1) btn1.setEnabled(false);
            if(inc > 1) store2 += sec_time;
            if(clk1 > 0 || clk2 > 0) cnt2.cancel();
            clk1++;
            cnt1 = new CountDownTimer(store1, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    store1 = millisUntilFinished;
                    int i = (int)(store1 / 1000) / 60;
                    int j = (int)(store1 / 1000) % 60;
                    double d1 = (i * 60 + j);
                    double d2 = time;
                    run1 = (d1 / (d2 * 1.0)) * 100000.0D;
                    prg1.setProgress((int)run1);
                    String str = String.format(Locale.ENGLISH, "%02d:%02d:%02d", Integer.valueOf(0), Integer.valueOf(i), Integer.valueOf(j));
                    txt1.setText(str);
                }

                @Override
                public void onFinish() {
                    btn1.setEnabled(false);
                    tst(getResources().getString(R.string.text_player_1));
                }
            }.start();
        });
        btn1.setOnClickListener(v -> {
            btn1.setEnabled(false);
            btn2.setEnabled(true);
            inc++;
            if(store2 < 1) btn2.setEnabled(false);
            if(inc > 1) store1 += sec_time;
            if(clk1 > 0 || clk2 > 0) cnt1.cancel();
            clk2++;
            cnt2 = new CountDownTimer(store2, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    store2 = millisUntilFinished;
                    int i = (int)(store2 / 1000) / 60;
                    int j = (int)(store2 / 1000) % 60;
                    double d1 = (i * 60 + j);
                    double d2 = time;
                    run2 = (d1 / (d2 * 1.0)) * 100000.0D;
                    prg2.setProgress((int)run2);
                    String str = String.format(Locale.ENGLISH, "%02d:%02d:%02d", Integer.valueOf(0), Integer.valueOf(i), Integer.valueOf(j));
                    txt2.setText(str);
                }

                @Override
                public void onFinish() {
                    btn2.setEnabled(false);
                    tst(getResources().getString(R.string.text_player_2));
                }
            }.start();
        });
    }

    @SuppressLint("MissingSuperCall")
    @Override
    public void onBackPressed() {
        Toast.makeText(MainActivity.this, R.string.backpress_toast_message, Toast.LENGTH_SHORT).show();
    }

    public void tst(String player){
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle(R.string.alertbox_quit_title);
        builder.setMessage(player + " " + getResources().getString(R.string.win_message));
        builder.setIcon(R.drawable.ic_baseline_add_alarm_24);
        builder.setPositiveButton(R.string.alertbox_okay_button, (dialog, which) -> finish());
        builder.create().show();
    }
}