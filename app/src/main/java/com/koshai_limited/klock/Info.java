package com.koshai_limited.klock;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.EditText;

public class Info extends AppCompatActivity {

    private Button btn1;
    private EditText txt1, txt2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        setContentView(R.layout.activity_info);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setHomeButtonEnabled(false);
        }

        txt1 = findViewById(R.id.txt_1);
        txt2 = findViewById(R.id.txt_2);
        btn1 = findViewById(R.id.submit);
        txt1.addTextChangedListener(textWatcher);
        txt2.addTextChangedListener(textWatcher);
        btn1.setOnClickListener(v -> {
            Intent intent = new Intent(Info.this, MainActivity.class);
            intent.putExtra("min", txt1.getText().toString());
            intent.putExtra("sec", txt2.getText().toString());
            startActivity(intent);
        });
    }

    @SuppressLint("MissingSuperCall")
    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(Info.this);
        builder.setTitle(R.string.alertbox_quit_title);
        builder.setMessage(R.string.alertbox_quit_message);
        builder.setIcon(R.drawable.baseline_report_24);
        builder.setPositiveButton(R.string.alertbox_positive_button, (dialog, which) -> finish());
        builder.setNegativeButton(R.string.alertbox_negative_button, (dialog, which) -> {});
        builder.create().show();
    }

    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String str1 = txt1.getText().toString();
            String str2 = txt2.getText().toString();
            btn1.setEnabled(!str1.isEmpty() && !str2.isEmpty());
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };
}