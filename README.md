# Klock

![License: GPL-3.0-or-later](https://img.shields.io/badge/License-GPL--3.0--or--later-004D40.svg)
&nbsp;&nbsp;
![release: 1.2.0](https://img.shields.io/badge/release-1.2.0-blue.svg)

<img src="fastlane/metadata/android/en-US/images/icon.png" alt="Klock" width="50">

A simple chess based clock application

<img alt="Klock Screenshots" src="fastlane/metadata/android/en-US/images/phoneScreenshots/3.png" width="220">

## what it is

Klock is a simple chess based clock. It is built to keep the minimal requirement for a clock application. We play chess locally anywhere. Sometimes we are badly in need of a clock. May be we don't need the complicated rules and play very easily. There Klock may come in handy.

It is a simple information. You don't need to be online for the app being functional. Just open the app, give your time configuration and that's it!

## build the app

See [BUILD_INFO.org](BUILD_INFO.org) file for details.

## how to use the app

See the [USAGE.org](USAGE.org) file for details.

## contributions

  - Add features by making a <a href="https://docs.codeberg.org/collaborating/pull-requests-and-git-flow/" target="_blank">pull request</a>.
  - Help to translate to your language by making a <a href="https://docs.codeberg.org/collaborating/pull-requests-and-git-flow/" target="_blank">pull request</a>.

## license

Klock - simple chess clock<br>
Copyright (C) 2021-present M.H. Rahman Kwoshik

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a [copy](LICENSE.txt) of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

<a href="https://f-droid.org/en/packages/com.koshai_limited.klock/" target="_blank">
  <img src="https://f-droid.org/badge/get-it-on.png" height="60"/>
</a>
&nbsp;
<a href="https://apt.izzysoft.de/fdroid/index/apk/com.koshai_limited.klock/" target="_blank">
  <img src="https://gitlab.com/IzzyOnDroid/repo/-/raw/master/assets/IzzyOnDroid.png" height="60"/>
</a>
